README
OG Django Rodo - Rodo info package

1. Add "og_django_rodo" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'og_django_rodo',
    ]


2. Add "og_django_rodo.context_processors.rodo_context" to your TEMPLATE in settings.py like this::

    'context_processors': [
        ...
        og_django_rodo.context_processors.rodo_context
    ],

3. In <head> section load CSS style: <link rel="stylesheet" href="{% static 'og_django_rodo/css/rodo-modal.css' %}" type="text/css">
4. In base template add: {% load rodo %} and  {% render_rodo_popup TEMPLATE BUTTON_COLOR_HEX %}
