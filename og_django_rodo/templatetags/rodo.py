# -*- coding: utf-8 -*-
import django
from django.template import Library, RequestContext
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from distutils.version import StrictVersion


register = Library()


@register.inclusion_tag('rodo/rodo-modal.html', takes_context=True)
def render_rodo_popup(context, rodo_content_path_in_project=None, hex_color=None):
    if StrictVersion(django.get_version()) < StrictVersion('1.8'):
        html_project_content = render_to_string(rodo_content_path_in_project, context_instance=context)
    else:
        if isinstance(context, RequestContext):
            context = context.flatten()
        html_project_content = render_to_string(rodo_content_path_in_project, context=context)
    return {
        'html_project_content': mark_safe(html_project_content),
        'hex_color': hex_color
    }
