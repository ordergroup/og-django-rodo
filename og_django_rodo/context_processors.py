# -*- coding: utf-8 -*-

# TODO stan ODRZUCIŁ/NIE WIDZIAŁ/ZAAKCEPTOWAŁ


def rodo_context(request):
    """
    Returns context variable depends on cookie rodo accepted flag.
    """
    try:
        rodo_cookie_value = bool(int(request.COOKIES.get('is_rodo_accepted', '0')))
    except ValueError:
        rodo_cookie_value = False

    return {
        'is_rodo_accepted': rodo_cookie_value
    }

