var projectName = 'og_django_rodo';

module.exports = {
    'projectName': projectName,
    'serverport': 4000,

    'styles': {
        'src': 'styles/**/*.scss',
        'dest': '../' +projectName + '/static/' + projectName + '/css',
        'config': {
          'outputStyle': 'compressed'
        }
    },
    'templates': {
        'path': 'templates',
        'watch': 'templates/**/*.html',
        'src': ['templates/**/*.html'],
        'dest': '../' + projectName + '/templates/rodo/'
    }
};
