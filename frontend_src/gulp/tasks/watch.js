var config = require('../config');

var gulp = require('gulp');

gulp.task('watch', [], function () {
    gulp.watch(config.styles.src, ['styles:dev']);
    gulp.watch(config.templates.src, ['templates:dev']);
});
