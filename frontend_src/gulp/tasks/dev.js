var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('default', function (callback) {
    callback = callback || function () {};
    runSequence('watch', 'styles:dev', 'templates:dev', callback);
});
